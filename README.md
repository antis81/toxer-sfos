# About Toxer for SailfishOS
This is the SailfishOS user interface implementation based on Toxer - a multi-platform Tox client framework with focus on QtQuick applications.

*Please note the app and framework is still highly being developed and may have major flaws. Neither can it be considered feature complete. If you feel like contributing to a fun and high quality messenger project, don't hesitate and open a merge request on Gitlab.*

How does it look like you ask? Here's some first impression I took from my phone:

<img src="webres/toxer.png" alt="Toxer-SFOS Start screen" style="width: 380px;"/>

And I may humbly add, that Toxer supports styling the application right away! If you like a more SailfishOS like native look and feel, just go to settings and pull the opacity trigger to the left - et voila. Have fun sailing. 😊

# HowTo build Toxer for SailfishOS

The following prerequisites are required, before you can build Toxer-SFOS:

* Make sure SailfishOS SDK (aka MerSDK) is installed and accessible via SSH.
* Clone this repository (Toxer-SFOS.git) to a directory of choice:

```bash
cd /my/projects
git clone https://gitlab.com/antis81/Toxer-SFOS.git
```

Having this setup, building Toxer for SailfishOS is fairly simple and straight forward.

## Step 1: Setup the SailfishOS SDK & Projekt

A default SDK environment is located in `mersdk/env`. Export the version constants to your needs. 

```bash
# Example: Edit SailfishOS target version
export SFVER='2.2.1.18'
# Now adjusting the toxcore & sodium versions is just as simple.
# Have a look inside the "mersdk/env" file to get all information.
```

To get you up to development work quickly I have prepared two scripts:

1. setup-mersdk.sh -> Setup the SailfishOS SDK virtual machine.
2. setup-toxer.sh -> Populate the Toxer project directory.

This will install the following packages for you inside the SDK:

Package(s) | Notes
---- | ----
gcc, gcc-c++ | C/C++ compiler
make | good old Make
cmake | CMake build tooling
autoconf, automake | required for libsodium
libtool | required for libsodium
git-minimal | required to use the Git hash as Toxer version

Run the preparation scripts:

```bash
# install required packages in MerSDK
# build & install toxcore + sodium libs in MerSDK before building the package
./setup-mersdk.sh

# copy the toxercore project sources (will be located in ToxerCore)
./setup-toxer.sh
```

Don't forget to run `setup-mersdk.sh` every time after upgrading SailfishOS SDK.

## Step 2: Build the RPM Package

To build the RPM package manually, SSH into MerSDK and call the `mb2` build command:

```bash
# ssh into the Build-SDK…
ssh -p 2222 -i ~/SailfishOS/vmshare/ssh/private_keys/engine/mersdk mersdk@localhost

# …and build the RPM package.
cd /home/src1/Toxer-SFOS  # or cd to /your/Toxer/sailfish directory
mb2 -t SailfishOS-<sfos_version>-<target> build
```
