#!/bin/bash
set -e

# note: vars valid on host system
SCRIPT_DIR=$(dirname `readlink -f $0`)
SSH_KEYDIR="~/SailfishOS/vmshare/ssh/private_keys/engine/mersdk"
extradir=$SCRIPT_DIR/extra # lib binaries go here

# source SDK environment
source $SCRIPT_DIR/mersdk/env

function mersdk_exec() {
# Runs a command inside (the running) MerSDK VM
# $1: the shell command
  ssh -p 2222 -i $SSH_KEYDIR mersdk@localhost "$1"
}

# Install additional dependencies in MerSDK
echo -en "Installing MerSDK build dependencies… \t"
mersdk_exec "sb2 -t SailfishOS-${SFVER}-i486    -m sdk-install -R zypper -n install gcc gcc-c++ autoconf automake make cmake libtool git-minimal" > /dev/null
mersdk_exec "sb2 -t SailfishOS-${SFVER}-armv7hl -m sdk-install -R zypper -n install gcc gcc-c++ autoconf automake make cmake libtool git-minimal" > /dev/null
echo "OK"

# Prepare build dir and script
echo -en "Building the Tox basis (toxcore, sodium)… \t"
mersdk_exec "mkdir -p ~/tox" > /dev/null
scp -P 2222 -i "$SSH_KEYDIR" $SCRIPT_DIR/mersdk/build-libs.sh mersdk@localhost:~/tox > /dev/null
scp -P 2222 -i "$SSH_KEYDIR" $SCRIPT_DIR/mersdk/env mersdk@localhost:~/tox > /dev/null
echo "OK"

# Build
mersdk_exec "cd ~/tox; ./build-libs.sh i486 && ./build-libs.sh armv7hl"

# Copy everything where it's needed
echo -en "Copying libs and headers \t"
mkdir -p $extradir
scp -r -P 2222 -i "$SSH_KEYDIR" "mersdk@localhost:~/tox/*i486" $extradir/i486 > /dev/null
scp -r -P 2222 -i "$SSH_KEYDIR" "mersdk@localhost:~/tox/*armv7hl" $extradir/armv7hl > /dev/null
# Clean up unneeded files
# i486
rm -rf $extradir/i486/bin > /dev/null
rm -rf $extradir/i486/lib/pkgconfig > /dev/null
#rm -f $extradir/i486/lib/*.so* > /dev/null
#rm -f $extradir/i486/lib/*.la > /dev/null
# arm
rm -rf $extradir/armv7hl/bin > /dev/null
rm -rf $extradir/armv7hl/lib/pkgconfig > /dev/null
#rm -f $extradir/armv7hl/lib/*.so* > /dev/null
#rm -f $extradir/armv7hl/lib/*.la > /dev/null
echo "OK"
