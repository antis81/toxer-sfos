import QtQuick 2.2

import style 1.0

Text {
    maximumLineCount: 1
    wrapMode: Text.NoWrap
    textFormat: Text.PlainText
    elide: Text.ElideRight
    color: Style.color.text
    font.pixelSize: Style.fontPointSize
}
