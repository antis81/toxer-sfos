import QtQuick 2.0
import Sailfish.Silica 1.0 as SF

import com.tox.qmlcomponents 1.0
import com.tox.qmltypes 1.0

import style 1.0

Rectangle {
    id: root
    implicitHeight: selfInfo.implicitHeight
    implicitWidth: selfInfo.implicitWidth
    color: Style.color.alternateBase

    ToxProfileQuery {
        id: tpq

        function statusIcon() {
            if (isOnline()) {
                switch (statusInt()) {
                case ToxTypes.Unknown:
                case ToxTypes.Away: return Style.icon.away;
                case ToxTypes.Busy: return Style.icon.busy;
                case ToxTypes.Ready: return Style.icon.online;
                }

                return Style.icon.away;
            } else {
                return Style.icon.offline;
            }
        }

        onIsOnlineChanged: {
            selfInfo.statusLight.source = statusIcon();
        }
        onStatusChanged: {
            selfInfo.statusLight.source = statusIcon();
        }
        onUserNameChanged: {
            selfInfo.name.text = userName;
        }
        onStatusMessageChanged: {
            selfInfo.statusMessage = statusMessage;
        }
    }

    FriendDelegate {
        id: selfInfo

        anchors.fill: parent
        anchors.margins: parent.height * 0.05
        anchors.bottomMargin: 3

        avatar.source: {
            var url = Toxer.avatarsUrl() + "/" + tpq.publicKeyStr().toUpperCase() + ".png"
            return Toxer.exists(url) ? url : Style.icon.noAvatar;
        }

        name.text: tpq.userName();
        statusMessage.text: tpq.statusMessage()
        statusLight.source: tpq.statusIcon()
    }
}
