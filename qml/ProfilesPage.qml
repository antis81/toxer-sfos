/*
* This file is part of the Toxer application, a Tox messenger client.
*
* Copyright (c) 2017-2018 Nils Fenner <nils@macgitver.org>
*
* This software is licensed under the terms of the MIT license:
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to
* deal in the Software without restriction, including without limitation the
* rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
* sell copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
* IN THE SOFTWARE.
*/

import QtQuick 2.5
import Sailfish.Silica 1.0 as SF
import QtGraphicalEffects 1.0

import animations 1.0 as Animations
import base 1.0 as Base
import controls 1.0 as Controls
import style 1.0

Base.Page {
    id: page

    Animations.Ringing {
        target: logo
        duration: 100
        loops: 12
        running: true
    }

    SF.SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height
        contentWidth: parent.width
        SF.PullDownMenu {
            SF.MenuItem {
                text: qsTr("Settings")
                onClicked: { pageStack.push(Qt.resolvedUrl("SettingsPage.qml")); }
            }
            SF.MenuItem {
                text: qsTr("Quit")
                onClicked: { Qt.quit(); }
            }
        }

        Column {
            id: column
            width: parent.width

            Image {
                id: logo
                anchors.horizontalCenter: column.horizontalCenter
                width: page.width / 3.0
                height: width
                sourceSize.width: width
                sourceSize.height: height
                source: Style.icon.toxerLogo
            }

            SF.Label {
                id: txtToxerVersion
                anchors.horizontalCenter: column.horizontalCenter
                text: Qt.application.name + " " + Qt.application.version
            }

            SF.Label {
                id: txtToxcoreVersion
                anchors.horizontalCenter: column.horizontalCenter
                text: "Based on Tox " + Toxer.toxVersionString()
            }

            ListView {
                id: pageSelector

                property real spikeWidth: 26
                property color highlightColor: "#09a"

                implicitHeight: contentItem.childrenRect.height
                implicitWidth: contentItem.childrenRect.width

                interactive: false
                orientation: ListView.Horizontal
                spacing: 6

                model: ListModel {
                    ListElement {
                        name: qsTr("Start Profile")
                        page: "SelectProfile.qml"
                    }
                    ListElement {
                        name: qsTr("New Profile")
                        page: "CreateProfile.qml"
                    }
                    ListElement {
                        name: qsTr("Import Profile")
                        page: ""
                    }
                }

                delegate: SF.Label {
                    height: paintedHeight + 36
                    width: implicitWidth + 36
                    horizontalAlignment: Qt.AlignHCenter
                    verticalAlignment: Qt.AlignVCenter
                    text: name

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            pageSelector.currentIndex = index
                        }
                    }
                }

                highlightFollowsCurrentItem: false
                highlight: Item {
                    Canvas {
                        id: highlighter
                        width: pageSelector.currentItem.width
                        height: pageSelector.currentItem.height
                        onPaint: {
                            var spikeWidth = pageSelector.spikeWidth;
                            var cy = height / 2;

                            var ctx = getContext("2d");
                            ctx.save();

                            ctx.beginPath();
                            ctx.moveTo(ctx.lineWidth, cy);
                            ctx.lineTo(spikeWidth, ctx.lineWidth);
                            ctx.lineTo(width - spikeWidth, ctx.lineWidth);
                            ctx.lineTo(width, cy);
                            ctx.lineTo(width - spikeWidth, height - ctx.lineWidth);
                            ctx.lineTo(spikeWidth, height - ctx.lineWidth);
                            ctx.lineTo(ctx.lineWidth, cy);
                            ctx.strokeStyle = pageSelector.highlightColor;
                            ctx.stroke();
                            ctx.fillStyle = pageSelector.highlightColor;
                            ctx.fill();

                            ctx.restore();
                        }

                        x: pageSelector.currentItem.x

                        Behavior on x {
                            SmoothedAnimation { duration: 180 }
                        }
                    }

                    Glow {
                        id: highlighter_effect
                        anchors.fill: highlighter
                        radius: 16
                        samples: 17
                        color: pageSelector.highlightColor;
                        source: highlighter
                    }

                    Animations.Pulsing {
                        target: highlighter_effect
                        properties: "spread"
                        from: 0.1
                        to: 0.4
                        durationIn: 180
                        durationOut: 300
                        loops: Animation.Infinite
                        running: true
                    }
                }

                onCurrentIndexChanged: {
                    var item = model.get(currentIndex);
                    pageLoader.source = Qt.resolvedUrl(item.page);
                }

                Component.onCompleted: {
                    currentIndex = 0
                }
            }
            Loader {
                id: pageLoader
                width: column.width
                height: 300
            }
        }
    }
}
